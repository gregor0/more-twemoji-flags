# More twemoji flags

I made a few flags in the style of the flags in the [twemoji](https://github.com/twitter/twemoji) emoji set.

Feel free to contact me with suggestions.

# They look like this

## LGBTQ+

| flag                                              |name             | colors                                          | Source                                                                                                             |
|---------------------------------------------------|-----------------|-------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|<img src="svg/rainbow-flag.svg" width="40">        | rainbow         | #880082 #3558A0 #138F3E #FAD220 #FF5000 #FF000E | [twemoji](https://github.com/twitter/twemoji/blob/master/assets/svg/1f3f3-fe0f-200d-1f308.svg)                     |
|<img src="svg/trans-pride-flag.svg" width="40">    | trans-pride     | #5BCEFA #F5A9B8 #EEEEEE #F5A9B8 #5BCEFA         | [twemoji](https://github.com/twitter/twemoji/blob/master/assets/svg/1f3f3-fe0f-200d-26a7-fe0f.svg)                 |
|<img src="svg/bisexual-pride-flag.svg" width="40"> | bisexual-pride  | #D60270 #9B4F96 #0038A8                         | vertical stripes template, colors from [here](https://en.wikipedia.org/wiki/Bisexual_pride_flag)                   |
|<img src="svg/pansexual-pride-flag.svg" width="40">| pansexual-pride | #ff218c #ffd800 #21b1ff                         | vertical stripes template, colors from [here](https://commons.wikimedia.org/wiki/File:Pansexuality_Pride_Flag.svg) |

## Country
| flag |name | note | Source |
|------|-----|------|--------|
|<img src="svg/bougainville_shiny.svg" width="40"> <img src="svg/bougainville_dull.svg" width="40"> | [Autonomous Region of Bougainville](https://en.wikipedia.org/wiki/Autonomous_Region_of_Bougainville)  | the shiny version has the orignal colors, the dull version is a bit desaturated. | [public domain flag from wikimedia](https://commons.wikimedia.org/wiki/File:Flag_of_Bougainville.svg) inserted in the template |


# The template?
Instead of manually changing colors of existing flags (i did that before), i made a template. To get the rounded corners, I used the orinal path from the flag of [japan](https://github.com/twitter/twemoji/blob/master/assets/svg/1f1ef-1f1f5.svg). This path is used in a clipPath, that is used as a clip-path. I used a nested svg with a different viewBox to avoid long decimal fractions in the dimensions.

